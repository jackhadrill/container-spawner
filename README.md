# Code Spawner

[![Build Status](https://drone.jacknet.io/api/badges/jackhadrill/container-spawner/status.svg)](https://drone.jacknet.io/jackhadrill/container-spawner)

A tool for spawning a bespoke container upon receiving an HTTP request.

## Usage

More details coming soon.

An example `docker-compose.yml` config is show below:

```yml
version: '3'
services:
  ...
  container-spawner:
    image: git.jacknet.io/jackhadrill/container-spawner:latest
    restart: always
    environment:
      CONTAINER_IMAGE: "git.jacknet.io/jackhadrill/code-server:latest"
      CONTAINER_PREFIX: "vscode"
      CONTAINER_NETWORK: "vscode_backend"
      CONTAINER_PERSIST: "/home/coder"
  ...
```

## Process description

Upon receiving any HTTP request, **Code Spawner** will launch a bespoke container on behalf of the user (if not already existing), equivalent to having used the command below. The user's name is derived from the `X-Forwarded-Preferred-User` header, original sent by [**OAuth2 Proxy**](https://github.com/oauth2-proxy/oauth2-proxy). 

```bash
$ docker run -d --rm --name ${CONTAINER_PREFIX}-${X-Forwarded-Preferred-User} -v ${CONTAINER_PREFIX}-${X-Forwarded-Preferred-User}:${CONTAINER_PERSIST} --network ${CONTAINER_NETWORK} ${IMAGE_NAME}
```

For example, assuming `X-Forwarded-Preferred-User` is `jack`:

```bash
$ export CONTAINER_IMAGE="git.jacknet.io/jackhadrill/code-server:latest"
$ export CONTAINER_PREFIX="vscode"
$ export CONTAINER_NETWORK="vscode_backend"
$ export CONTAINER_PERSIST="/home/coder"
$ docker run -d --rm --name vscode-jack -v vscode-jack:/home/coder --network vscode_backend git.jacknet.io/jackhadrill/code-server:latest
```
