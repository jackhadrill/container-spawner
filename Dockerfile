FROM python:3-alpine

ENV CONTAINER_IMAGE="git.jacknet.io/jackhadrill/code-server:latest"
ENV CONTAINER_PREFIX="vscode"
ENV CONTAINER_NETWORK="vscode_backend"
ENV CONTAINER_PERSIST="/home/coder"

WORKDIR /src
RUN pip install --no-cache-dir --extra-index-url https://git.jacknet.io/api/packages/jackhadrill/pypi/simple containerspawner

CMD ["containerspawner"]
