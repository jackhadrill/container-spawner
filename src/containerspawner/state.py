"""State management for Container Spawner."""
import os
import time
from typing import List
import docker
from docker.models.containers import Container

CONTAINER_IMAGE = os.environ.get("CONTAINER_IMAGE") or "codercom/code-server:latest"
CONTAINER_PREFIX = os.environ.get("CONTAINER_PREFIX") or "vscode"
CONTAINER_NETWORK = os.environ.get("CONTAINER_NETWORK") or "vscode_backend"
CONTAINER_PERSIST = os.environ.get("CONTAINER_PERSIST") or "/home/coder"


class StateManager:
    """Store container states."""
    def __init__(self) -> None:
        self._docker: docker.DockerClient = docker.from_env()
        self._spawned_containers: List[str] = []

    def is_running(self, username: str) -> bool:
        """Determines if the user's container is running.

        :param username: The username to check.
        :returns: True if contaienr exists.
        """
        container_name = CONTAINER_PREFIX + "-" + username
        return container_name in [
            container.name for container in self._docker.containers.list(all=True)
        ]

    def spawn(self, username: str) -> None:
        """Spawn a new Docker container for the specified user.

        :param username: The username of the user.
        """
        if self.is_running(username):
            return

        container_name = CONTAINER_PREFIX + "-" + username
        self._spawned_containers.append(container_name)
        print(f"Spawning {container_name}!")
        container: Container = self._docker.containers.run(
            image=CONTAINER_IMAGE,
            name=container_name,
            network=CONTAINER_NETWORK,
            volumes={container_name: {"bind": CONTAINER_PERSIST}},
            detach=True,
            auto_remove=True
        )

        # Wait for container to start.
        while container.status != "running":
            time.sleep(1)

        time.sleep(3)

        # Disable auth.
        container.exec_run("sed -i 's/auth: password/auth: none/' ~/.config/code-server/config.yaml")

        # Install extensions.
        container.exec_run("code-server --install-extension ms-python.python")

        # Restart container.
        container.restart()
