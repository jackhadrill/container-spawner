"""Launch Container Spawner."""
import typer
from waitress import serve
from containerspawner import app


def cli(
    host: str = typer.Option(
        "0.0.0.0",
        envvar=["CONTAINER_SPAWNER_HOST"],
        help="Host for Container Spawner to listen on."
    ),
    port: str = typer.Option(
        "8080",
        envvar=["CONTAINER_SPAWNER_PORT"],
        help="Port for Container Spawner to listen on."
    )
):
    """Run Container Spawner application using Waitress."""
    try:
        serve(app, host=host, port=port)
    except OSError as exception:
        print(str(exception))
        raise typer.Exit(code=1)


def main() -> None:
    """Run CLI parser."""
    typer.run(cli)


if __name__ == "__main__":
    main()
