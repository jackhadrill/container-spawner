"""Catch all web server to spawn Docker containers."""
from flask import Flask, make_response, request
from containerspawner.state import StateManager

HTTP_METHODS = ["GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH"]

app = Flask(__name__)
state = StateManager()


@app.route("/", defaults={"path": ""}, methods=HTTP_METHODS)
@app.route("/<path:path>", methods=HTTP_METHODS)
def default(path):
    """Catch all endpoint to spawn Docker containers."""
    username = request.headers.get("X-Forwarded-Preferred-Username")
    if not username:
        return make_response("No username provided by upstream.", 400)

    if not state.is_running(username):
        state.spawn(username)

    response = make_response(f"Container spawned. Reloading{'' if path else ' ' + path}...", 201)
    response.headers["Refresh"] = "2"
    return response
